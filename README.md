# bitcoin-project

## Description
This project aims to process bitcoin data and summaraise it.
## Usage
Clone this repository and copy data file ('btc 5.csv') to your project directory. Use commands below to build docker image and run the container to get the results in your project directory.

```
docker build -t bitcoin-img .
```

```
docker run -it --name bitcoin-container -v ${PWD}:/root/app/ bitcoin-img
```