"""Process bitcoin data

This script filters and summarises a few years of Bitcoin pricing data. In particular it:
    1. Converts Low and High columns to Euro.
    2. Saves past year data (365 days) to new csv file.
    3. Prints summary table with min, max and mean for Low (Eur), High (Eur) and Volume.
    4. Generates linegraph of past year data for Low (Eur), High (Eur) and saves an image.

It accepts comma separated file (.csv) with following columns:
- Date,
- Open,
- High,
- Low,
- Close,
- Adj Close,
- Volume.

"""

import pandas as pd

import sys


def convert_usd2eur(df: pd.DataFrame) -> pd.DataFrame:
    """Convert USD to EUR with a static conversion rate
    
    Args:
        df (pd.DataFrame): The dataframe with columns: Low, High

    Returns
        df (pd.DataFrame): The dataframe with additional columns: Low (EUR), High (EUR)
    """
    conversion_rate = 0.87
    df.loc[:,'Low (EUR)'] = conversion_rate * df['Low']
    df.loc[:,'High (EUR)'] = conversion_rate * df['High']
    return df

def get_past_year(df: pd.DataFrame) -> pd.DataFrame:
    """Filter last 365 days of data and save it to csv file

    Args:
        df (pd.DataFrame): The dataframe with column Date and additional columns to be saved in new file

    Returns
        df (pd.DataFrame): The dataframe filtered to only past year data
    """
    df.loc[:,'Date'] = df['Date'].astype('datetime64')
    max_date = max(df['Date'])
    df.loc[:,'Diff'] = (max_date - df['Date']).dt.days
    df_past_year = df[df['Diff'] < 365]
    df_past_year.drop(columns=['Diff'], inplace=True)
    df_past_year.reset_index(drop=True).to_csv("past_year_data.csv", index=False)
    return df_past_year

def print_summary_table(df: pd.DataFrame) -> None:
    """Print summary table with min, max and mean for Low (EUR), High (EUR) and Volume (Billion)
    
    Args:
        df (pd.DataFrame): The dataframe with columns: Low (EUR), High (EUR), Volume
    """
    df.loc[:,'Volume (Billion)'] = df['Volume'] / 1000000
    print(df[['Low (EUR)', 'High (EUR)', 'Volume (Billion)']].agg(['min','max','mean']).round(2).transpose())

def generate_img(df: pd.DataFrame) -> None:
    """Generate linegraph of the Bitcoin price (Low & High) in the past year and save it to file.
    
    Args:
        df (pd.DataFrame): The dataframe with columns: Date, Low (EUR), High (EUR)
    """
    df[['Date','Low (EUR)', 'High (EUR)']].plot(x='Date',
                                            figsize=(15,9),
                                            title = 'Bitcoin price (Low & High) in the past year').get_figure().savefig('linegraph.png')

def main(inputfile: str) -> None:
    
    df = pd.read_csv(inputfile)
    df = convert_usd2eur(df)
    df_past_year = get_past_year(df[['Date','Volume','Low (EUR)','High (EUR)']]) 
    print_summary_table(df_past_year[['Volume','Low (EUR)','High (EUR)']])   
    generate_img(df_past_year)
    
if __name__ == "__main__":
    main(sys.argv[1])