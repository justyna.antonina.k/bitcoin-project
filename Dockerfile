# basic layer -- miniconda
FROM continuumio/miniconda3

# init conda
RUN conda init bash && \
    conda config --set ssl_verify false && \
    echo "source activate base" > ~/.bashrc

# set workdir
ENV PROJECT_ROOT /root/app
WORKDIR $PROJECT_ROOT

COPY environment.yml environment.yml
RUN conda env update -n base -f environment.yml && conda clean --all -y

# copy code to the image
COPY . $PROJECT_ROOT

ENTRYPOINT [ "python3", "main.py" ]
CMD [ "btc 4.csv" ]